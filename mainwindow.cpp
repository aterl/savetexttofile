#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTextDocumentWriter>
#include <QMessageBox>
#include <QFileDialog>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QString path = QFileDialog::getExistingDirectory(0, "Select folder to save", "");
    QString fileName = QFileDialog::getSaveFileName(this, "Save Text", path, "*.txt ;; *.doc ;; *.docx ;; *.odt ;; *");
    if(fileName.isEmpty()) {
        return;
    }
    QTextDocumentWriter writer(fileName);
    if(writer.write(ui->textEdit->document())) {
        QMessageBox::information(this, "Success", "Your text has been saved!");
    }
    else {
        QMessageBox::critical(this, "Fail", "Sorry, some error with save.");
    }
}
